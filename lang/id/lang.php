<?php

return [
    'list' => [
        'restore_selected' => 'Pulihkan terpilih',
        'restore_selected_empty' => 'Tidak ada rekam dipilih untuk di pulihkan.',
        'restore_selected_confirm' => 'Pulihkan rekam terpilih?',
        'restore_selected_success' => 'Rekam terpilih telah dipulihkan.',
    ],
];