<?php

return [
    'list' => [
        'restore_selected' => 'Restore selected',
        'restore_selected_empty' => 'There are no selected records to restore.',
        'restore_selected_confirm' => 'Restore the selected records?',
        'restore_selected_success' => 'Restored selected records.',
    ],
];