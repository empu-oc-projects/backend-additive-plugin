<?php

namespace Empu\BackendAdditive;

use Backend\Classes\Controller as BackendController;
use Backend\Facades\Backend;
use Empu\BackendAdditive\ReportWidgets\HeroWidget;
use Illuminate\Support\Facades\App;
use October\Rain\Support\Facades\Config;
use System\Classes\PluginBase;

/**
 * BackendAdditive Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Backend Additive',
            'description' => 'Collection of additonal features for OctoberCMS backend',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        App::register(\Maatwebsite\Excel\ExcelServiceProvider::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        BackendController::extend(function ($controller) {
            $styles = ['~/plugins/empu/backendadditive/assets/styles/tropic.scss'];

            if (Config::get('cms.backendUseSunnyStyle', true)) {
                array_push($styles, '~/plugins/empu/backendadditive/assets/styles/sunny.scss');
            }

            $controller->addCss($controller->combineAssets($styles));
            $controller->addJs(
                $controller->combineAssets([
                    '~/plugins/empu/backendadditive/assets/js/additive.js',
                    '~/plugins/empu/backendadditive/assets/js/autoNumeric-min.js',
                    '~/plugins/empu/backendadditive/assets/js/select2/i18n/id.js',
                ])
            );
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\BackendAdditive\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.backendadditive.some_permission' => [
                'tab' => 'BackendAdditive',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'backendadditive' => [
                'label'       => 'BackendAdditive',
                'url'         => Backend::url('empu/backendadditive/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.backendadditive.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            FormWidgets\MonthSelect::class => 'monthselect',
            FormWidgets\StaticText::class => 'static',
            FormWidgets\StromClearfix::class => 'clearfix',
            FormWidgets\AutoNumeric::class => 'autonumeric',
        ];
    }

    public function registerReportWidgets()
    {
        return [
            HeroWidget::class => [
                'label'   => 'Application Identity',
                'context' => 'dashboard',
                'permissions' => [],
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'timediff' => [Classes\ListColumnTypes::class, 'timeDiff'],
            'softdelete' => [Classes\ListColumnTypes::class, 'softdeleteStatus'],
        ];
    }
}
